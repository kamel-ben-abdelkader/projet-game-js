# **Project-Game-JS**

Le Project-Game-JS à pour but de realiser en deux semaines un jeu developper en javascript dans le cadre d'un projet d'étude.

J'ai choisi de realiser un Jeu "Memory" sur le theme du jeux vidéo Street Fighter.


Programme Javascript pour créer le jeu du Memory consistant à retrouver les paires d'images mémorisées au début de la partie par clic sur les images. 
Le joueur doit tenter de mémoriser l'emplacement d'un maximum d'images. L'internaute doit cliquer sur une première case et une seconde. Si les images sont identiques, le code Javascript les valide et les conserve affichées.
Le cas échéant, après un délai réduit, il les masque à nouveau.
Chaque tentative est incrementer et l'EventListener sur les clics est réinitialisé pour pouvoir vérifier la prochaine association. Le score est incrementer à chaque bonne réponse. 
Lorsque toutes les associations d'images ont été retrouvées, le nombre de tentative final est révélé au joueur. 


Je vous invite à tester cette premiere version.


https://kamel-ben-abdelkader.gitlab.io/projet-game-js

## Comment jouer.

Commencez le jeu en cliquant sur une des cartes. Trouvez les cartes identitiques en un minimum de tentative.


 ## ScreenShot du jeu:


<br> Interface

<br><img src = "/public/img/maquette-memory1.jpg" width="300" height="300" />

<br>Images cartes 

<br><img src = "/public/img/maquette-memory2.jpg" width="300" height="300" />

<br> Jeux Terminer

<br><img src = "/public/img/maquette-memory3.jpg" width="300" height="300" />

## Technologies Utiliser.

- Javascript.

- CSS.

- HTML.


## Fonctionement.

Le jeu est entierement baser sur des fonctions Javascript.

Le principe est tres simple , par exemple une function vas disposer aléatoirement mes images.


Exemples ci-dessous:

```javascript
function shuffleCards() {
    cards.forEach((card) => {
        let randomPosition = Math.floor(Math.random() * 18);
        card.style.order = randomPosition;
    })
};


```

## Version.

Actuellement en version 0.3.6

 Des améliorations et des ajouts sont susceptible d'etre apporter prochainement comme:

- Rajout d'une section Start 
- Rajout d'un timer avec un game over 


## Auteur.

@Kamel, formation développeur web et mobile chez Simplon, promo14.

## **Contact**.

Pour plus d'information ou de simple questions n'hesitez pas a me contactez via mail : kamel.benabdelkader69@gmail.com

Vous pouvez aussi me retrouvez sur [Simplonlyon.fr.](https://www.simplonlyon.fr/)


