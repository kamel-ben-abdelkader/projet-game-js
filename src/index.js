import {Modal} from './Modal'

const cards = document.querySelectorAll('.card');
const board = document.querySelector('.game-board');
const buttonAgain = document.querySelector('.again');

let hasFlippedCard = false;
let firstCard;
let secondCard;
let lockBoard = false;
let points = 0;
const bgMusic = new Audio('sounds/themesound.mp3');
const youWin = new Audio('sounds/you-win.mp3');
const flip_card = new Audio('sounds/flip-card.mp3');
const scored = new Audio('sounds/goodsong.mp3');
const wrong = new Audio('sounds/wrong.mp3');
let attempts = 0;





function shuffleCards() {
    cards.forEach((card) => {
        let randomPosition = Math.floor(Math.random() * 18);
        card.style.order = randomPosition;
    })
};


shuffleCards()


function flipCard() {
    if(lockBoard) return;
    if(this === firstCard) return;
    this.classList.add('flip');
    flip_card.play();
    if(!hasFlippedCard) {
        hasFlippedCard = true;
        firstCard = this;
        return;
    }
    secondCard = this;
    flip_card.play();
    hasFlippedCard = false;
    checkForMatch();
}

function checkForMatch() {
    if(firstCard.dataset.card === secondCard.dataset.card) {
        disabledCards();
        scored.play();
        points ++;
        attempts ++;
        if (points == 9) {
            document.querySelector('#attempts')
            .innerHTML = attempts; 
            setTimeout(() => {
                board.classList.add('board-off');
            }, 500);
            setTimeout(() => {
                youWin.play();
                modal.open();
                buttonAgain.addEventListener('click', ()=> {
                    playAgain()
                })
            }, 2000);
        }
        return;
    } 
    unflipCards();
}

function disabledCards() {
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);
    resetBoard();
}

function unflipCards() {
    lockBoard = true;
    setTimeout(() => { 
        wrong.play();
        attempts++;
    }, 600);
    setTimeout(() => { 
        firstCard.classList.remove('flip');
        secondCard.classList.remove('flip');
        flip_card.play();
        resetBoard();
    }, 1500);
}

function resetBoard() {
    [hasFlippedCard, lockBoard] = [false, false];
    [firstCard, secondCard] = [null, null];
}




function playAgain() {
    document.location.reload();
}

cards.forEach((card) => {
    card.addEventListener('click', flipCard)
});




//================= AUDIO ====================================//


document.body.addEventListener('click', ()=> {
    bgMusic.volume = 0.2;
    bgMusic.play() 
})



//================= section Class Modal====================================//


let modal = new Modal
modal.close();


//================= class CARDS en cours  ====================================//



// class Cards {
//     dataCard;
//     dataClass;

//     constructor(paramDataCard, paramDataClass){
//     this.dataCard = paramDataCard;
//     this.dataClass= paramDataClass;

//     }



    
// toHTML(filenameCardFace,filenameCardBack){

//     let elementDiv = document.createElement('div');
//     elementDiv.classList.add(this.dataClass);
//     elementDiv.setAttribute( 'data-card',this.dataCard);
//     board.appendChild(elementDiv);
    
//     this._aspectRecto= document.createElement('img');
//     this._aspectRecto.src = filenameCardFace;
//     this._aspectRecto.classList.add('card-face');
//     elementDiv.appendChild(this._aspectRecto)



//     this._aspectVerso= document.createElement('img');
//     this._aspectVerso.src = filenameCardBack;
//     this._aspectVerso.classList.add('card-back');
//     elementDiv.appendChild(this._aspectVerso);

// }

// }



// let carteRyu = new Cards('ryu','card')
//     carteRyu.toHTML("img/ryu.jpg", "img/cardback.png")

// let carteHonda = new Cards('ehonda','card')
//     carteHonda.toHTML(".img/ehonda.png", "img/cardback.png")

// let carteBlanka = new Cards('blanka','card')
//     carteBlanka.toHTML("img/blanka.jpg", "img/cardback.png")
    
   

//     const cards = [{carteRyu,carteHonda,carteBlanka]}];


//     // (function shuffleCards(cards) {
//     //     for (const item of cards ){
//     //         let randomPosition = Math.floor(Math.random() * 18);
//     //         item.style.order = randomPosition;
//     //         bgMusic.play();
            
//     //     }
        
//     //     })(cards);
